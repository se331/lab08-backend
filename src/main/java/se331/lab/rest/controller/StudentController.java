package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    ArrayList<Student> students;
    public StudentController(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("692115001")
                .name("Somsak")
                .surname("Jeamterasakul")
                .gpa(1.12)
                .image("https://i.imgur.com/QvZjgU3.jpg")
                .penAmount(112)
                .description("ไม่แนะนำให้แชร์นะครับ")
                .build());
        this.students.add(Student.builder()
                .id(2l)
                .studentId("692115002")
                .name("Taksin")
                .surname("Shinawatra")
                .gpa(4.00)
                .image("https://i.imgur.com/OuHMLRq.jpg")
                .penAmount(30)
                .description("I bought the whole universe")
                .build());
        this.students.add(Student.builder()
                .id(3l)
                .studentId("692115003")
                .name("Justin")
                .surname("Bieber")
                .gpa(10.00)
                .image("https://i.imgur.com/zhL2WCe.jpg")
                .penAmount(10)
                .description("I love holidays")
                .build());
        this.students.add(Student.builder()
                .id(4l)
                .studentId("692115004")
                .name("Prayuht")
                .surname("Chan-o-cha")
                .gpa(1.10)
                .image("https://i.imgur.com/9zmoPjJ.jpg")
                .penAmount(10)
                .description("The dictator")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(students);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }

}
